module edu.ntnu.idatt2001.paths {
  requires javafx.controls;
  requires javafx.media;
  requires java.desktop;
  requires java.logging;

  opens edu.ntnu.idatt2001.paths to javafx.fxml;
  exports edu.ntnu.idatt2001.paths to javafx.graphics; // Keep this line
  exports edu.ntnu.idatt2001.paths.model to javafx.graphics;
  opens edu.ntnu.idatt2001.paths.model to javafx.fxml;
  // Add this line to export the view package to javafx.graphics
  exports edu.ntnu.idatt2001.paths.view to javafx.graphics;
  opens edu.ntnu.idatt2001.paths.view to javafx.fxml;
  exports edu.ntnu.idatt2001.paths.model.player to javafx.graphics;
  opens edu.ntnu.idatt2001.paths.model.player to javafx.fxml;
  exports edu.ntnu.idatt2001.paths.model.game to javafx.graphics;
  opens edu.ntnu.idatt2001.paths.model.game to javafx.fxml;
}
